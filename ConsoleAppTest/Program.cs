using ConsoleAppTest.Models.BinaryHeap;
using ConsoleAppTest.Models.Graph;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace ConsoleAppTest {
    class Program {
        static void Main(string[] args) {
            
            Graph graph =new Graph();

            Vertex v1 =new Vertex(1);
            Vertex v2 =new Vertex(2);
            Vertex v3 =new Vertex(3);
            Vertex v4 =new Vertex(4);
            Vertex v5 =new Vertex(5);
            Vertex v6 =new Vertex(6);
            Vertex v7 =new Vertex(7);
            

            graph.AddVertex(v1);
            graph.AddVertex(v2);
            graph.AddVertex(v3);
            graph.AddVertex(v4);
            graph.AddVertex(v5);
            graph.AddVertex(v6);
            graph.AddVertex(v7);
            


            graph.AddEdges(v1, v2, 1);
            graph.AddEdges(v1, v3, 1);
            graph.AddEdges(v3, v4, 1);
            graph.AddEdges(v1, v2, 1);
            graph.AddEdges(v2, v5, 1);
            graph.AddEdges(v2, v6, 1);
            graph.AddEdges(v5, v6, 1);
            graph.AddEdges(v5, v2, 1);
            graph.AddEdges(v6, v5, 1);
            graph.AddEdges(v6, v2, 1);
            
            int[,] matrix = graph.GetMatrix();

           for(int i =0; i <graph.VertexCount; i++) {
                Console.Write((i+1) +"   ");
                for(int j =0; j <graph.VertexCount; j++){

                    Console.Write(matrix[i, j] +" ");
                }
                Console.WriteLine();
            }
           Console.WriteLine();
           Console.Write("    ");
           for(int i =0; i<graph.VertexCount; i++) {
                Console.Write(i+1 +" ");
            }
            Console.WriteLine();


            Console.WriteLine("�������:");
            Console.WriteLine();

            List<Vertex> res;
            Console.WriteLine(graph.Deep(v1, v6, out res));
            foreach(Vertex v in res) {
                Console.Write(v +" ");
            }
            Console.WriteLine();

            Console.WriteLine(graph.Deep(v1, v7, out res));
            foreach(Vertex v in res) {
                Console.Write(v +" ");
            }

            Console.WriteLine();
        }        
    }
}
