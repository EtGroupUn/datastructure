﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.Models {
    class EasyMap<TKey, TValue> :IEnumerable {

        private List<MapItem<TKey, TValue>> mapItems =new List<MapItem<TKey, TValue>>();
        private List<TKey> Keys =new List<TKey>();

        public EasyMap() {

        }

        public void Add(MapItem<TKey, TValue> mapItem) {

            if (!Keys.Contains(mapItem.Key)) {
                Keys.Add(mapItem.Key);
                mapItems.Add(mapItem);
            }
        }


        public TValue Search (TKey key) {

            if (Keys.Contains(key)) {


                return mapItems.Single(i =>i.Key.Equals(key)).Value;
            } else {
                return default(TValue);
            }

        }
    
        public void Remove (TKey key) {
            if (Keys.Contains(key)) {
                mapItems.Remove( mapItems.Single(i =>i.Key.Equals(key)));
                Keys.Remove(key);
            }
        }

        public IEnumerator GetEnumerator() {
            return mapItems.GetEnumerator();
        }
    }
}
