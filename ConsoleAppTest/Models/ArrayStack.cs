﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.Models {
    class ArrayStack<T> {
        T[] items;

        
        private int currentIndex =0;
        public int Count => currentIndex;

        public ArrayStack(int size =10) {
            items =new T [size];
        }

        public ArrayStack(T data, int size =10) : this(size) {
            items[currentIndex] =data;
            currentIndex++;
        }

        public void Push(T data) {
            currentIndex++;
            items[currentIndex] =data;
        }


        public T Pop() {
            if(currentIndex > 0) {
                var item = items[currentIndex];
                items[currentIndex] =default(T);
                currentIndex--;
                return item;
            } else {
                throw new Exception("Стек переполнен");
            }
        }


        public T Peek() {
            if(currentIndex > 0) {
                return items[currentIndex];
            } else {
                throw new Exception("Стек пуст");
            }
        }
    }
}
