﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.Models.BinaryHeap {
    class BinaryHeap<Int32> :IEnumerable{
        
        private List<int> items =new List<int>();
        public int Count => items.Count;

        public BinaryHeap() {
            
        }

        public BinaryHeap(List<int> newList) {
            items.AddRange(newList);

            for(int i =Count; i >=0; i--) {
                Sort(i);
            }
        }


        public void Add(int itm) {

            items.Add(itm);

            int currentIndex =Count -1;
            int parentIndex =GetParentIndex(currentIndex);

            while((currentIndex >0) && (items[parentIndex] < items[currentIndex])) {
                Swap(parentIndex, currentIndex);

                currentIndex =parentIndex;
                parentIndex =GetParentIndex(currentIndex);
            }
        }


        private int GetMax() {
            int result =items[0];
            items[0] =items[Count -1];
            items.RemoveAt(Count -1);
            Sort(0);
            return result;
        }

        private void Sort(int currentIndex) {
            

            while(currentIndex < Count) {
                int leftIndex, rightIndex;
                int maxIndex =currentIndex;

                leftIndex =2 *currentIndex +1;
                rightIndex =2 *currentIndex +2;

                if(leftIndex <Count && items[leftIndex] > items[maxIndex]) {
                    maxIndex =leftIndex;
                }
                if(rightIndex <Count && items[rightIndex] > items[maxIndex]) {
                    maxIndex =rightIndex;
                }

                if(currentIndex == maxIndex) {
                    break;
                }

                Swap(maxIndex, currentIndex);
                currentIndex =maxIndex;
            }
        }

        private void Swap(int firstIndex, int secondIndex) {
            int temp =items[firstIndex];
            items[firstIndex] =items[secondIndex];
            items[secondIndex] =temp;
        }


        private int GetParentIndex(int currentIndex) {
            return (currentIndex -1) /2;
        }

        public IEnumerator GetEnumerator() {
            while(Count > 0) {
                yield return GetMax();
            }
        }
    }
}
