﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.Models.PrefixTree {
    class Trie<T> {

        public Node<T> root;
        public int Count { get; private set;}


        public Trie() {
            root = new Node<T>('\0', default(T), "");
            Count =1;
        }

        public void Add(string key, T data) {
            AddNode(key, data, root);
        }


        private void AddNode(string key, T data, Node<T> node) {
            if (string.IsNullOrEmpty(key)) {
                node.IsWord =true;
                node.Data =data;
            } else {
                char simbol =key[0];
                Node<T> thisNode =node.TrySearch(simbol);
                if(thisNode != null) {
                    AddNode(key.Substring(1), data, thisNode);
                } else {
                    Node<T> newNode = new Node<T>(key[0], data, node.Prefix +key[0]);
                    node.SubNodes.Add(key[0], newNode);
                    AddNode(key.Substring(1), data, newNode);
                }
            }
        }


        public void Remove(string key) {
            RemoveNode(key, root);
        }


        private void RemoveNode(string key, Node<T> node) {
            if (string.IsNullOrEmpty(key)) {
                node.IsWord =false;
                Count --;
            } else {
                char simbol =key[0];
                Node<T> subNode =node.TrySearch(simbol);
                if(subNode != null) {
                    RemoveNode(key.Substring(1), subNode);
                }
            }
        }


        public bool Search(string key, out T value) {
            return SearchNode(key, root, out value);
        }

        private bool SearchNode(string key, Node<T> node, out T value) {
            bool result =false;
            value =default(T);
            if (string.IsNullOrEmpty(key)) {
                if(node.IsWord ==true)
                    result =true;
                    value =node.Data;
            } else {
                char simbol =key[0];
                Node<T> subnode =node.TrySearch(simbol);
                if(subnode != null) {
                    result =SearchNode(key.Substring(1), subnode, out value);
                } 
            }
            return result;
        }
    }
}
