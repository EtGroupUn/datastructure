﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.Models.PrefixTree {
    class Node <T> {

        public char Simbol { get; set;}
        public T Data { get; set;}
        public string Prefix { get; set;}
        public bool IsWord { get; set;}

        public Dictionary<Char, Node<T>> SubNodes {get; set; }

        public Node(char simbol, T data, string prefix) {
            Simbol =simbol;
            Data =data;
            Prefix =prefix;

            SubNodes =new Dictionary<char, Node<T>>();
        }

        public Node<T> TrySearch(char key) {
            if(SubNodes.TryGetValue(key, out Node<T> value)){
                return value;
            }
            return null;
        }

        public override string ToString() {
            return Data.ToString();
        }
    }
}
