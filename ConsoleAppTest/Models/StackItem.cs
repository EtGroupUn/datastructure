﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.Models {
    class StackItem <T> {

        private T data = default(T);
        private StackItem<T> previous = null;

        public T Data {
            get{
                return data;
            } 
            set{
                if(value != null) {
                    data = value;
                }
            } 
        }
        public StackItem<T> Previous { 
            get{
                return previous;
            } 
            
            set{ 
                if(value != null) {
                    previous =value;
                }
            } 
        }

        public StackItem(T data) {
            if(data != null) {
                Data =data;
            }
        }

        public override string ToString() {
            return Data.ToString();
        }

    }
}
