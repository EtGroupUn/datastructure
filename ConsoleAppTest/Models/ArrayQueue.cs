﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.Models {
    class ArrayQueue<T> {

        T [] items;

        public int Count { get; private set;}
        
        private T Head => items[Count -1];
        private T Tail => items[0];

        private int MaxCount => items.Length;


        public ArrayQueue(int size) {
            items =new T[size];
        }

        public ArrayQueue(int size, T data) {
            items =new T[size];
            items[0] =data;
            Count =1;
        }

        public void Enqueue( T data) {
            if (Count < MaxCount) {
                T[] result = new T[MaxCount];
                result[0] = data;
                for(int i =0; i < Count; i++) {
                    result[i +1] =items[i];
                }
                items =result;
                Count++;
            }
        }


        public T Dequeue() {
            
            T item =Head;
            Count --;
            return item;
        }

        public T Peek() {
            return Head;
        }

    }
}
