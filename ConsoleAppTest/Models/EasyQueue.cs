﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.Models {
    class EasyQueue<T> {

        public List<T> items =new List<T>();

        private T Tail =>items.Last();

        private T Head =>items.First();

        public EasyQueue() { }

        public EasyQueue(T data) {
            items.Add(data);
        }

        public void Enqueue(T data) {
            items.Add(data);
        }

        public T Dequeue() {
            
            T item = Head;
            items.Remove(item);
            return item;
        }

        public T Peek() {
            return Head;
        }

    }
}
