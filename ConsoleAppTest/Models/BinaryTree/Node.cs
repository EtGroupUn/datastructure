﻿using System;


namespace ConsoleAppTest.Models.BinaryTree {
    class Node<T>:IComparable
        where T : IComparable {
        
        public T Data { get; private set;}
        public Node<T> Left { get; private set;}
        public Node<T> Right { get; private set;}
        
        public Node(T data) {
            Data =data;
        }

        public Node(T data, Node<T> left, Node<T> right) {
            Data =data;
            Left =left;
            Right =right;
        }


        public int CompareTo(object obj) {
            if(obj is Node<T> item) {
                return Data.CompareTo(item);
            } else {
                throw new Exception();
            }
        }
        
        public void Add(T data) {

            if(data.CompareTo(Data) == -1) {
                if(Left == null) {
                    Left =new Node<T>(data);
                } else {
                    Left.Add(data);
                }
            } else {
                if(Right == null) {
                    Right =new Node<T>(data);
                } else {
                    Right.Add(data);
                }
            }
        }
    }
}
