﻿using System;
using System.Collections.Generic;

namespace ConsoleAppTest.Models.BinaryTree {
    class BinaryTree<T>
        where T: IComparable{

        public Node<T> Root { get; private set;}
        public Int32 Count { get; private set;}

        public void Add(T data) {
            
            Node<T> node = new Node<T>(data);
            if(Root == null) {
                Root =node;
                Count =1;
                return;
            }
            Root.Add(data);
            Count++;
        }


        public List<T> Preorder() {

            if(Root == null) {
                return new List<T>();
            }
            
            return Preorder(Root);
        }


        public List<T> Preorder(Node<T> node) {
            List<T> ts =new List<T>();
            if(node != null) {
                ts.Add(node.Data);
                if(node.Left != null) {
                    ts.AddRange(Preorder(node.Left));
                }
                if(node.Right != null) {
                    ts.AddRange(Preorder(node.Right));
                }
            }

            return ts;
        }


        public List<T> Postorder() {

            if(Root == null) {
                return new List<T>();
            }
            
            return Postorder(Root);
        }


        public List<T> Postorder(Node<T> node) {
            List<T> ts =new List<T>();
            if(node != null) {
                if(node.Left != null) {
                    ts.AddRange(Postorder(node.Left));
                }
                if(node.Right != null) {
                    ts.AddRange(Postorder(node.Right));
                }
                ts.Add(node.Data);
            }

            return ts;
        }


        public List<T> Inorder() {

            if(Root == null) {
                return new List<T>();
            }
            
            return Inorder(Root);
        }


        public List<T> Inorder(Node<T> node) {
            List<T> ts =new List<T>();
            if(node != null) {
                if(node.Left != null) {
                    ts.AddRange(Inorder(node.Left));
                }
                ts.Add(node.Data);
                if(node.Right != null) {
                    ts.AddRange(Inorder(node.Right));
                }
            }

            return ts;
        }
    }
}
