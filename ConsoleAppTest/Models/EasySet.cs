﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.Models {
    public class EasySet<T> :System.Collections.IEnumerable {

        private List<T> items =new List<T>();

        public int Count => items.Count;

        public EasySet() { }

        public EasySet(T data) {
            items.Add(data);
        }


        public void Add(T data) {
            foreach( T temp in items) {
                if(temp.Equals(data)) {
                    return;
                }
            }
            items.Add(data);
        }

        public void Remove(T data) {
            items.Remove(data);
        }

        public EasySet<T> Union(EasySet<T> inSet) {
           EasySet<T> union =new EasySet<T>();

            foreach(T tmp in items) {
                union.Add(tmp);
            }

            foreach(T tmp in inSet.items) {
                union.Add(tmp);
            }
            return union;
        }

        public EasySet<T> Vychitanie(EasySet<T> inSet) {
            EasySet<T> thisEasySet =new EasySet<T>();
            bool flag =false;

            foreach(T temp0 in items) {
                flag =false;
                foreach(T temp1 in inSet.items) {
                    if (temp0.Equals(temp1)) {
                        flag =true;
                        break;
                    }
                }
                if(flag == false) {
                    thisEasySet.Add(temp0);
                }
            }
            return thisEasySet;
        }


        public EasySet<T>Peresechenie(EasySet<T> inSet) {
            EasySet<T> set =new EasySet<T>();

            foreach(T temp0 in items) {
                foreach(T temp1 in inSet) {
                    if (temp0.Equals(temp1)) {
                        set.Add(temp0);
                    }
                }
            }

            return set;
        }


        public bool SubSet(EasySet<T> inSet)  {
            
            foreach(T temp in items) {
                
                    if (!inSet.items.Contains(temp)) {
                        return false;
                    } else {
                        break;
                    }
                }
            return true;
        }

        public IEnumerator GetEnumerator() {
            return items.GetEnumerator();
        }
    }
}
