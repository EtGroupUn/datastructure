﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.Models {
    class EasyStack <T> {

        List<T> items =new List<T>();
        Int32 Count =>items.Count();

        public void Push(T data) {
            items.Add(data);
        }

        public T Pop() {

            if(items.Count > 0) {
                var item = items.Last();
                items.Remove(item);
                return item;
            } else {
                throw new Exception("Стек пуст");
            }
        }


        public T Peek() {
            if(items.Count > 0) {
                return items.Last();
            } else {
                throw new Exception("Стек пуст");
            }
        }

        public override string ToString() {
            return $"Стек с {Count} элементами";
        }
    }
}
