﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.Models {
    class LinkedStack<T> {

        public StackItem<T> Head { get; private set;}
        public Int32 Count {get; private set;}

        public LinkedStack() { 
            Head =null;
            Count =0;
        }

        LinkedStack(T data) {
            Head.Data =data;
            Count ++;
        }

        public void Push(T data) {
            StackItem<T> stackItem =new StackItem<T>(data);
            stackItem.Previous =Head;
            Head =stackItem;
            Count++;
        }

        public T Pop() {
            if (Count > 0) {
                StackItem<T> stackItem =Head;
                Head = Head.Previous;
                Count --;
                return stackItem.Data;
            } else {
                throw new Exception("Стек пуст");
            }
        }


        public T Peek() {
            if(Count > 0) {
                return Head.Data;
            } else {
                throw new Exception("Стек пуст");
            }
        }



    }
}
