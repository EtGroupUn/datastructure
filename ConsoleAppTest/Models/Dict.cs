﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ConsoleAppTest.Models {
    class Dict<TKey, TValue> :IEnumerable {

        Int32 size =50;
        MapItem<TKey, TValue>[] mapItems;
        List<TKey> keys;

        public Dict() {
            mapItems =new MapItem<TKey, TValue>[size];
            keys =new List<TKey>();
        }


        public void Add(MapItem<TKey, TValue> item) {
            
            Int32 hash = GetHashCode(item.Key);
            if(mapItems[hash] == null) {
                keys.Add(item.Key);
                mapItems[hash] =item;
            } else {
                bool placed =false;
                for(Int32 i = hash; i <size; i++) {
                    if(mapItems[i] == null) {
                        keys.Add(item.Key);
                        mapItems[i] =item;
                        placed =true;
                        break;
                    }
                }

                if (!placed) {
                    for(Int32 i =0; i <hash; i++) {
                        if(mapItems[i] == null) {
                            keys.Add(item.Key);
                            mapItems[i] =item;
                        }
                    }
                }

                if (!placed) {
                    throw new Exception("Массив словаря полностью забит");
                }
            }
        }

        public TValue Search(TKey key) {
            if (keys.Contains(key)) {
                Int32 hash = GetHashCode(key);
                if (mapItems[hash].Key.Equals(key)) {
                    return mapItems[hash].Value;
                } else {
                    for(Int32 i =hash; i <size; i++) {
                        if (mapItems[i].Key.Equals(key)) {
                            return mapItems[i].Value;
                        }
                    }

                    for(Int32 i =0; i <hash; i++) {
                        if (mapItems[i].Key.Equals(key)) {
                            return mapItems[i].Value;
                        }
                    }
                }
                return default (TValue);
            } else {
                return default (TValue);
            }
        }

        public void Remove(TKey key) {
            if (keys.Contains(key)) {
                Int32 hash =GetHashCode(key);  
                if(mapItems[hash] != null && mapItems[hash].Key.Equals(key)) {
                    keys.Remove(mapItems[hash].Key);
                    mapItems[hash] =null;
                    return;
                } else {
                    for(Int32 i =hash +1; i <size; i++) {
                        if (mapItems[i].Key.Equals(key)) {
                            keys.Remove(mapItems[i].Key);
                            mapItems[i] =null;
                            return;
                        }
                    }
                    for(Int32 i =0; i <hash; i++) {
                        if (mapItems[i].Key.Equals(key)) {
                            keys.Remove(mapItems[i].Key);
                            mapItems[i] =null;
                            return;
                        }
                    }
                }
            } 
        }








        private Int32 GetHashCode(TKey key) {
            return key.GetHashCode() %size;
        }

        public IEnumerator GetEnumerator() {
            foreach(MapItem<TKey, TValue> it in mapItems) {
                if(it != null) {
                    yield return it;
                }
            }
        }
    }
}
