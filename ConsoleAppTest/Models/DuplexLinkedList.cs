﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ConsoleAppTest.Models {
    class DuplexLinkedList<T> :IEnumerable<T>{

        public DuplexItem<T> Head { get; set;}
        public DuplexItem<T> Tail { get; set;}

        private int Count =0;

        public DuplexLinkedList() { }

        public DuplexLinkedList(T data) {

        }

        void SetHeadAndTail( T data) {
            DuplexItem<T> item = new DuplexItem<T>(data);
            Head =item;
            Tail =item;
            Count =1;
        }

        public void Add(T data) {
            
            if(Count == 0) {
                SetHeadAndTail(data);
                return;
            }

            DuplexItem<T> item = new DuplexItem<T>(data);
            Tail.Next =item;
            item.Previous = Tail;
            Tail = item;
            Count ++;
        }

        public void Delete(T data) {
            DuplexItem<T> current= Head.Next;
            if (Head.Data.Equals(data)) {
                Head =Head.Next;
                Count --;
                return;
            }

            while(current != null) {
                if (current.Data.Equals(data)) {
                    current.Previous.Next = current.Next;
                    current.Next.Previous =current.Previous;
                    Count --;
                    return;
                }
                current =current.Next;
            }
        }

        public DuplexLinkedList<T> Reverse() {

            DuplexLinkedList<T> result =new DuplexLinkedList<T>();
            DuplexItem<T> current = Tail;

            while(current != null) {
                result.Add(current.Data);
                current =current.Previous;
            }
            return result;
        }

        public IEnumerator GetEnumerator() {
            
            DuplexItem<T> item =Head;
            while(item != null) {
                yield return item;
                item =item.Next;
            }
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator() {
            return (IEnumerator<T>)GetEnumerator();
        }
    }
}
