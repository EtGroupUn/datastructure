﻿using System;


namespace ConsoleAppTest.Models {
    class LinkQueueItem<T> {

        public T Data { get; set;}

        public LinkQueueItem<T> Next { get; set;}

        public LinkQueueItem(T data) {
            Data =data;
        }
    }
}
