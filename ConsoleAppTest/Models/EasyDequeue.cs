﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.Models {
    class EasyDequeue<T> {
        List<T> items =new List<T>();

        private T Head => items.Last();

        private T Tail => items.First();
        private Int32 Count => items.Count();

        public EasyDequeue() {}

        public EasyDequeue(T data) {
            items.Add(data);
        }

        public void PushBack(T data) {
            items.Insert(0, data);
        }

        public void PushFront(T data) {
            items.Add(data);
        }

        public T PopBack() {
            T item = items.First();
            items.Remove(item);
            return item;
        }


        public T PopFront() {
            T item =items.Last();
            items.Remove(item);
            return item;
        }


        public T PeekBack() {
            return items.First();
        }

        public T PeekFront() {
            return items.Last();
        }
    }
}
