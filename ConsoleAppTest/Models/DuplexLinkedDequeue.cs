﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.Models {
    class DuplexLinkedDequeue<T> {

        private DuplexItem<T> head =null;
        private DuplexItem<T> tail =null;

        public Int32 Count { get; set;}

        public DuplexLinkedDequeue() {}

        public DuplexLinkedDequeue(T data) {
            SetHeadAndTail(data);
        }

        private void SetHeadAndTail(T data) {
            DuplexItem<T> item = new DuplexItem<T>(data);

            head = item;
            tail = item;
            Count = 1;
        }

        public  void PushFront (T data) {
            if (Count == 0) {
                SetHeadAndTail(data);
                return;
            } else {
                DuplexItem<T> item =new DuplexItem<T>(data);
                item.Previous =head;
                head.Next =item;
                head =item;
                Count++;
            }
        }


        public void PushBack(T data) {
            if (Count == 0) {
                SetHeadAndTail(data);
                return;
            } else {
                DuplexItem<T> item =new DuplexItem<T>(data);
                item.Next =tail;
                tail.Previous =item;
                tail =item;
                Count++;
            }
        }

        public T PopFront() {
            if(Count == 1) {
                T result =head.Data;
                head =null;
                Count--;
                return result;
            }
            if(Count > 0) {
                DuplexItem<T> item =head;
                head.Previous.Next =null;
                head = head.Previous;
                Count --;
                return item.Data;
            } else {
                return default(T);
            }
            
        }


        public T PopBack() {
            if(Count == 1) {
                T result =tail.Data;
                tail =null;
                Count--;
                return result;
            }
            if(Count > 0) {
                DuplexItem<T> item =tail;
                tail.Next.Previous =null;
                tail =tail.Next;
                Count --;
                return item.Data;
            } else {
                return default(T);
            }
            
        }

        public T PeekFront() {
            return head.Data;
        }
        public T PeekBack() {
            return tail.Data;
        }
    }
}
