﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.Models {
    class LinkedQueue<T> {

        LinkQueueItem<T> head { get; set;}
        
        LinkQueueItem<T> tail { get; set;}

        public Int32 Count { get; private set;}

        public LinkedQueue() {}

        public LinkedQueue(T data) {
            SetHeadItem(data);
        }

        private void SetHeadItem(T data) {
            LinkQueueItem<T> item =new LinkQueueItem<T>(data);
            head =item;
            tail =item;
            Count =1;
        }

        public void Enqueue(T data) {
            if(Count == 0) {
                SetHeadItem(data);
                return;
            } else {
                LinkQueueItem<T> item =new LinkQueueItem<T>(data);
                item.Next =tail;
                tail =item;
                Count++;
            }
        }



        public T Dequeue() {

            T data = head.Data;

            LinkQueueItem<T> current = tail.Next;
            LinkQueueItem<T> previous = tail;
            while (current != null && current.Next != null) {
                previous = current;
                current = current.Next;
            }

            head = previous;
            head.Next =null;
            Count--;
            return data;
        }


        public T Peek() {
            return head.Data;
        }

    }
}
