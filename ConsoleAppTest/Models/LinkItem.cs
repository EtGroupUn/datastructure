﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.Models {
    public class LinkItem<T> {

        private T data =default(T);
        private LinkItem<T> next =null;

        public T Data { 
            get{ 
                return data;
            } 
            
            
            set{
                if(value != null) {
                    data = value;
                }
            } 
        }

        public LinkItem<T> Next { 
            get{
                return next;
            } 
            
            
            set{
                if(value != null) {
                    next =value;
                }
            }
        }



        public LinkItem(T data) {
            if(data != null) {
                Data =data;
            }
        }

        public override string ToString() {
            return Data.ToString();
        }

    }
}
