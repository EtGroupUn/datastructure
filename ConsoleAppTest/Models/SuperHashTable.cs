﻿using System;

namespace ConsoleAppTest.Models {
    public class SuperHashTable<T> {

        private HashItem<T>[] items;

        public SuperHashTable(int size) {
            items =new HashItem<T>[size];

            for(int i =0; i <items.Length; i++) {
                items[i] =new HashItem<T>(i);
            }
        }


        public void Add(T item) {
            int k = GetHash(item);

            items[k].Nodes.Add(item);
        }

        public bool Search(T item) {
            return items[GetHash(item)].Nodes.Contains(item);
        }



        private int GetHash(T item) {

            return item.GetHashCode() %items.Length;
        }
    }
}
