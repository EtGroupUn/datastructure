﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.Models {
    class CircleLinkedList<T> :IEnumerable {

        public DuplexItem<T> Head { get; set;}

        public Int32 Count { get; private set;}

        public CircleLinkedList() { }

        public CircleLinkedList(T data) {
            SetHeadItem(data);
        }



        public void Add(T data) {

            if(Count == 0) {
                SetHeadItem(data);
                return;
            } else {

                DuplexItem<T> item=new DuplexItem<T>(data);
                Head.Previous.Next =item;
                item.Next =Head;
                item.Previous =Head.Previous;
                Head.Previous =item;
                Count++;
            }
        }

        public void Delete(T data) {

            DuplexItem<T> current =Head.Next;
            if (Head.Data.Equals(data)) {
                RemoveItem(Head);
                Head = Head.Next;
                Count --;
            }

            for(int i =0; i <Count; i++) {
                if (current.Data.Equals(data)){
                    RemoveItem(current);
                    Count --;
                }
                current = current.Next;
            }
        }

        void RemoveItem(DuplexItem<T> current) {
            current.Next.Previous = current.Previous;
            current.Previous.Next =current.Next;
        }

        void SetHeadItem(T data) {
            Head = new DuplexItem<T>(data);
            Head.Next =Head;
            Head.Previous =Head;
            Count =1;
        }

        public IEnumerator GetEnumerator() {
            
            DuplexItem<T> current =Head;
            for(int i =0; i <Count; i++) {
                yield return current;
                current = current.Next;
            }
                
        }
    }
}
