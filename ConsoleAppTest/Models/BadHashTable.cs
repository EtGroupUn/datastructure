﻿using System;

namespace ConsoleAppTest.Models {
    public class BadHashTable<T> {
        private T[] items;

        public BadHashTable(int size) {
            items =new T[size];
        }

        public void Add(T item) {
            var key =GetHash(item);
            items[key] =item;
        }


        private int GetHash(T item) {
            return item.ToString().Length %items.Length;
        }



        public T Search(T item) {
            return items[GetHash(item)];

        }
    }
}
