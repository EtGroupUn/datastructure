﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.Models.Graph {
    class Vertex {

        public int Number { get; set;}

        public Vertex() { 
        }

        public Vertex(int number) {
            Number =number;
        }


        public override string ToString() {
            return Number.ToString();
        }

    }
}
