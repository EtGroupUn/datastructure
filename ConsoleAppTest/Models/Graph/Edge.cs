﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.Models.Graph {
    class Edge {

        public Vertex From =new Vertex();
        public Vertex To =new Vertex();
        public int Weight;


        public Edge(Vertex from, Vertex to, int weigh) {
            From =from;
            To =to;
            Weight =weigh;
        }

        public override string ToString() {
            return $"({From};{To})";
        }
    }
}
