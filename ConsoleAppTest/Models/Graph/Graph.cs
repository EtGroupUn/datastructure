﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.Models.Graph {
    class Graph {

        List<Vertex> Vertexes =new List<Vertex>();
        List<Edge> Edges = new List<Edge>();

        public int VertexCount =>Vertexes.Count();
        public int EdgesCount =>Edges.Count();

        public void AddVertex(Vertex vertex) {
            Vertexes.Add(vertex);
        }

        public void AddEdges(Vertex from, Vertex to, int weight) {
            Edges.Add(new Edge(from, to, weight));
        }

        public int[,] GetMatrix() {
            
            int[,] matrix = new int[VertexCount, VertexCount];

            foreach(Edge edge in Edges) {
                int row =edge.From.Number -1;
                int count =edge.To.Number -1;

                matrix[row, count] =edge.Weight;
            }
            return matrix;
        }


        public List<Vertex> GetListVertex(Vertex vertex) {

            List<Vertex> result =new List<Vertex>();

            foreach(Edge edge in Edges) {
                if(edge.From == vertex) {
                    result.Add(edge.To);
                }
            }

            return result;
        }

        public bool Wave(Vertex start, Vertex finish, out List<Vertex> result) {

            List<Vertex> list =new List<Vertex> {
                start
            };

            for(int i =0; i <list.Count; i++) {
                Vertex vertex =list[i];
                foreach(Vertex vx in GetListVertex(vertex)) {
                    if (!list.Contains(vx)){
                        list.Add(vx);
                    }
                }
            }

            
            result =list;
            return list.Contains(finish);
        }

        List<Vertex> list =new List<Vertex>();
        public bool Deep(Vertex start, Vertex finish, out List<Vertex> result) {
            if (!list.Contains(start)) {
                list.Add(start);            
            }
            

            for(int i =0; i < list.Count; i++) {
                Vertex vertex =list[i];

                if(GetListVertex(vertex).Count >0) {
                    foreach(Vertex vx in GetListVertex(vertex)) {
                        if (!list.Contains(vx)) {
                            list.Add(vx);
                            Deep(vx, finish, out list);
                        }
                    }
                }
            }
                

            result =list;

            return list.Contains(finish);
        }


    }
}
