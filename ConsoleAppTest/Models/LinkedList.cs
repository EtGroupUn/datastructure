﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppTest.Models {
    class LinkedList <T> :IEnumerable{

        public LinkItem<T> Head { get; private set;}
        public LinkItem<T> Tail { get; private set;}

        public Int32 Count { get; private set;}


        public LinkedList() {
            Head =null;
            Tail =null;
            Count =0;
        }


        void SetHeadAndTail(T data) {
            LinkItem<T> item =new LinkItem<T>(data);

            Head =item;
            Tail =item;
            Count++;

        }
        public LinkedList(T data) {
               SetHeadAndTail(data);
        }

        public void Add(T data) {
            
            if( Tail != null) {
                LinkItem<T> item = new LinkItem<T>(data);
                Tail.Next =item;
                Tail =item;
                Count++;
            } else {
                SetHeadAndTail(data);
            }
        }


        public void Delete(T data) {
            if(Head.Data != null) {

                var current =Head.Next;
                LinkItem<T> previous =Head;

                while (current.Next !=null) {
                    if (current.Data.Equals(data)) {

                        previous.Next =current.Next;
                        Count --;
                    }
                    previous =current;
                    current =current.Next;
                    
                }
            }


        }

        public void AddToHead(T data) {
            if(Head != null) {
                LinkItem<T> current =new LinkItem<T>(data);

                current.Next = Head;
                Head =current;
                Count++;
            }
        }


        public void InsertAfter(T target, T data) {
            if(Head != null) {

                LinkItem<T> current =Head.Next;
                LinkItem<T> previous =Head;
                if (previous.Data.Equals(target)) {
                    LinkItem<T> item =new LinkItem<T>(data);
                    item.Next = previous;
                    previous.Next =item;
                    return;
                }
                
                while(current != null) {
                    if (current.Data.Equals(target)) {
                        LinkItem<T> item =new LinkItem<T>(data);
                        item.Next =current.Next;
                        current.Next =item;
                    }
                    current =current.Next;
                    
                }

                

            }
        }

        public IEnumerator GetEnumerator() {
            LinkItem<T> current =Head;
            while(current != null) {

                yield return current.Data;
                current =current.Next;
            }
        }
    }




}
